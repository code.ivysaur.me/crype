# crype

![](https://img.shields.io/badge/written%20in-C%2B%2B%2C%20C-blue)

Deterministic syncable/diffable encryption, for files or pipes.

`crype` is a tool providing rsyncable deterministic-encryption (like rsyncrypto for pipes). It encrypts data using a common decision function over plaintext, allowing rsync-like tools to still work efficiently over the encrypted data. It uses HMAC-SHA1-PBKDF2 and the ChaCha stream cipher.

Compression is also implemented, as this improves both portabiity and rsyncability compared to a separate `gzip --rsyncable` pass.

See `crype --help` for usage instructions. Binaries are provided for 32-bit Windows, 32-bit Cygwin, and 64-bit Debian 7. 

## License

Source code is made available freely under the terms of the ISC license. The source code distribution includes library code under {public-domain, BSD, Unlicense} licenses.

The provided binaries are likewise freely redistributable for any purpose.

## TODO

- Reduce peak memory usage by using stream functions for decompression
- Parallel compression/encryption
- Distro packages, static binaries

## Changelog

2014-05-24: r59
- Add file header for version compatibility detection
- Add plaintext comment to file header
- Allow configuring compression level
- Support Debian 7 (amd64)
- Support Cygwin (i386)
- Security: Improve security by switching chunk IVs based on truncated plaintext HMAC (incompatible change)
- Security: Read encryption passwords from file
- Security: Warn when reading encryption password from argument
- [⬇️ crype-r59.zip](dist-archive/crype-r59.zip) *(205.77 KiB)*


2014-05-14: r30
- Initial release
- [⬇️ crype-r30.zip](dist-archive/crype-r30.zip) *(133.55 KiB)*

